<?php
/**
 * Sushi Worpdress Starter System Library
 *
 * Admin Functions
 *
 * @author Sushi Katana team
 * @copyright 2013 Sushi Digital Pty. Ltd.
 * @since Sashimi 3.0
 * @package WordPress
 * @subpackage Sushi_WP
 */

function swp_get_package_data( $package_file, $markup = true )
{
	$def_headers = array(
		'UniqueID'		=> 'Unique ID',
		'Name'			=> 'Package Name',
		'Description'	=> 'Description',
		'Version'		=> 'Version',
		'Authors'		=> 'Authors'
	);

	$package_data = get_file_data( $package_file, $def_headers );

	return $package_data;
}

function swp_get_packages( $packages_folder = '' )
{
	$packages_root = SWP_PACKAGES_DIR;
	$packages_url = SWP_PACKAGES_URL;
	$packages = array();

	if ( ! empty( $packages_folder ) ) {
		$packages_root .= $packages_folder;
		$packages_url .= $packages_folder;
	}

	$packages_dir = @opendir( $packages_root );

	while( ( $dir = readdir( $packages_dir) ) !== false ) {

		if ( substr( $dir, 0, 1) == '.' )
			continue;

		$package_dir = $packages_root . '/' . $dir;

		if ( is_dir( $package_dir ) ) {
			
			if ( file_exists( $package_dir . '/package.php' ) ) {

				$package_data = swp_get_package_data( $package_dir . '/package.php' );

				foreach ( array( 'png', 'jpg', 'gif' ) as $ext ) {
					if ( ! file_exists( $package_dir . '/screenshot.' . $ext ) ) {
						continue;
					} else {
						$screenshot = $packages_url . '/' . $dir . '/screenshot.' . $ext;	
						break;
					}
				}

				if ( empty( $package_data['UniqueID'] ) )
					$package_data['UniqueID'] = $dir;

				$package = new stdClass();
				$package->data = $package_data;
				$package->dir = sanitize_path( $package_dir );
				$package->image = $screenshot;

				$packages[] = $package;
			}
		}
	}

	return $packages;
}


/*
* END OF FILE
* functions.php
*/
?>