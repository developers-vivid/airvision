<?php
/**
 * Sushi Worpdress Starter System
 *
 * Configuration file.
 *
 * @author Sushi Katana team
 * @copyright 2013 Sushi Digital Pty. Ltd.
 * @since Sashimi 3.0
 * @package WordPress
 * @subpackage Sushi_WP
 */

function swp_admin_init()
{
	// Load library default dashboard style override
	swp_add_css( 'sushi-dashboard', swp_assets_url( '/styles/dashboard.css' ) );

	add_action( 'admin_enqueue_scripts', 'swp_load_admin_scripts' );
	add_filter( 'update_footer', 'swp_update_footer' );	
}

function swp_load_admin_scripts()
{
	swp_load_css( 'sushi-dashboard' );
	swp_load_css( 'sushi-admintheme-dashboard' );

	swp_load_js( 'jquery' );	
	swp_load_js( 'sushi-plugins' );
	
	swp_print_scripts();

	if ( isset($_GET['page']) )
	{
		switch ( $_GET['page'] )
		{
			case 'syslib-overview':
				swp_load_js( 'jquery-ui-core' );
				swp_load_js( 'jquery-ui-draggable' );
				swp_load_js( 'jquery-ui-droppable' );
				swp_load_js( 'postbox');
				add_thickbox();
				break;
			default:
				break;
		}
	}
}

function swp_admin_footer_text( $text )
{
	return __( 'Brought to you by Sushi Digital Pty. Ltd. <a href="http://sushidigital.com.au" target="_blank">http://www.sushidigital.com.au</a>.', 'swp' );
}

function swp_update_footer( $text )
{
	global $system;

	return sprintf( 'System Version: <a href="#" onclick="return false;">%s %s</a>', $system['model'], $system['version'] );
}

function swp_admintheme_url( $file = '' )
{
	$url = SWP_ADMINTHEMES_URL . '/' . SWP_CURRENTTHEME_URL;

	if ( !empty( $file ) )
		$url .= '/' . ltrim( $file, '/' );

	return $url;
}

function remove_editor_menu()
{
  remove_action('admin_menu', '_add_themes_utility_last', 101);
}
add_action('_admin_menu', 'remove_editor_menu', 1);


/*
* END OF FILE
* admin.php
*/

?>