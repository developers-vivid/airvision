<?php
/**
 * Sushi Worpdress Starter System Library
 *
 * URL and DIR functions.
 *
 * @author Sushi Katana team
 * @copyright 2013 Sushi Digital Pty. Ltd.
 * @since Sashimi 3.0
 * @package WordPress
 * @subpackage Sushi_WP
 */

function swp_upload_dir( $index )
{
	$indeces = array( 'path', 'url', 'subdir', 'basedir', 'baseurl', 'error' );	
	if ( in_array( $index, $indeces ) ) {
		$udir = wp_upload_dir();
		return trim( str_replace( '\\', '/', $udir[$index] ) );
	}	
	return NULL;
}

function swp_assets_dir( $file = '' )
{
	$dir = SWP_ASSETS_DIR;

	if ( ! empty( $file ) )
		$dir .= '/' . ltrim( $file, '/' );
	
	return ( file_exists( $dir ) ) ? $dir : NULL;
}

function swp_assets_url( $file = '' )
{
	$url = SWP_ASSETS_URL;

	if ( !empty( $file ) )
		$url .= '/' . ltrim( $file, '/' );

	return $url;
}

function swp_timthumb_url()
{
	if ( ! defined( 'SWP_TIMTHUMB_URL' ) )
		return SWP_ASSETS_URL . '/scripts/timthumb/timthumb.php';
		
	return SWP_TIMTHUMB_URL;
}

/*
* END OF FILE
* links.php
*/
?>