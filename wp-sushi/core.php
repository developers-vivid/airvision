<?php
/**
 * Sushi Worpdress Library
 *
 * General Template Functions
 *
 * @author Sushi Katana team
 * @copyright 2013 Sushi Digital Pty. Ltd.
 * @since Sashimi 3.0
 * @package WordPress
 * @subpackage Sushi_WP
 */

function swp_init()
{
	swp_init_filters();

	swp_addnload_js( 'sushi-plugins', SWP_ASSETS_URL . '/scripts/jquery.sushi.min.js', array( 'jquery' ) );

	swp_load_admintheme();
	
	if ( is_admin() ) 
	{
		require( SWP_ADMIN_DIR . '/functions.php' );
		require( SWP_ADMIN_DIR . '/admin.php' );

		add_action( 'admin_init', 'swp_admin_init' );
		add_filter( 'admin_footer_text', 'swp_admin_footer_text' );		
	}
}

function swp_init_filters()
{	
	add_action( 'swp_print_scripts', 'swp_print_scripts' );
}

function swp_load_admintheme()
{
	/* Set active admin theme. */
	$active_theme = get_option( SWPO_ACTIVE_ADMINTHEME );
	
	if ( $active_theme === false )
	{
		if ( file_exists( SWP_ADMINTHEMES_DIR . '/' . SWP_STARTER_DEFAULT_ADMINTHEME . '/theme.php' ) )
			$active_theme = SWP_STARTER_DEFAULT_ADMINTHEME;
		else
			$active_theme = 'wordpress';

		add_option( SWPO_ACTIVE_ADMINTHEME, $active_theme );
	}	

	if ( $active_theme != 'wordpress' )
	{
		$theme_file = SWP_ADMINTHEMES_DIR . '/' . $active_theme . '/theme.php';
		
		if ( file_exists( $theme_file ) )
		{
			require( $theme_file );
			define( 'SWP_LOGIN_THEME', $active_theme );

			do_action( 'swp_admintheme_init' );
			do_action( 'swp_admintheme_loaded' );	
		}
		else
		{
			// resets back to normal wordpress login
			update_option( SWPO_ACTIVE_ADMINTHEME, 'wordpress' );
		}
	}
}

function swp_print_scripts()
{
	echo create_favicon_link( get_template_directory_uri() . '/favicon.ico' ) . "\n";
	echo ie_conditional( 'lt', 9, '<script src="' .  SWP_ASSETS_URL . '/scripts/html5shiv.js"></script>
<style type="text/css">
	* { *behavior: url( "' .  SWP_ASSETS_URL . '/scripts/boxsizing.htc" ); }
</style>' );
}

function swp_load_late_scripts()
{
	if ( is_admin_bar_showing() ) {
		swp_load_css( 'sushi-admintheme-dashboard', SWP_CURRENTTHEME_URL . '/css/dashboard.css' );
	}
	
	swp_addnload_css( 'global-stylesheet', get_template_directory_uri() . '/css/global.css' );	
	swp_addnload_js( 'global-js', get_template_directory_uri() . '/js/global.js', array( 'jquery' ), true );
}


function swp_add_admintheme( $id, $name, $description, $author, $author_url, $version )
{
	global $sushiwp;

	if ( $sushiwp['adminthemes'] ) {

		foreach ( $sushiwp['adminthemes'] as $theme ) {
			if ( $theme['id'] == $id )
				return false;
		}

		$theme = array(
			'id'			=> $id,
			'name' 			=> $name,
			'decription' 	=> $description,
			'author'		=> $author,
			'author_url'	=> $author_url,
			'version' 		=> $version
		);

		array_push( $sushiwp['adminthemes'], $theme );

		return $theme;
	}

	return false;
}

function swp_add_package( $id, $name, $description, $version )
{
	global $sushiwp;

	if ( $sushiwp['packages'] ) {

		foreach ( $sushiwp['packages'] as $package ) {
			if ( $package['id'] == $id )
				return false;
		}

		$package = array(
			'id'			=> $id,
			'name' 			=> $name,
			'decription' 	=> $description,
			'version' 		=> $version
		);

		array_push( $sushiwp['packages'], $package );

		return $package;
	}

	return false;
}

/**
 * Setup the sushi wp theme.
 */
function swp_after_theme_setup()
{	
	// This theme uses a custom image size for featured images, displayed on "standard" posts.
	add_theme_support( 'post-thumbnails' );
	// Enable HTML5 output for the search form
	add_theme_support( 'html5', array('search-form') );	
	// This theme uses wp_nav_menu() in one location.
	register_nav_menu( 'primary', 'Main Navigation' );
	// After theme setup extension
	do_action( 'swp_after_theme_setup' );
}
add_action( 'after_setup_theme', 'swp_after_theme_setup' );

/**
 * Register our sidebars and widgetized areas.
 */
function swp_arphabet_widgets_init() {

	register_sidebar( array(
		'name' => 'Sidebar',
		'id' => 'sidebar_1',
		'before_widget' => '<div>',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="rounded">',
		'after_title' => '</h2>',
	) );
}
add_action( 'widgets_init', 'swp_arphabet_widgets_init' );

/*
* END OF FILE
* core.php
*/
?>