<?php
/**
 * Sushi Worpdress Starter System Library
 *
 * Loads classes and other includes.
 *
 * @author Sushi Katana team
 * @copyright 2013 Sushi Digital Pty. Ltd.
 * @since Sashimi 3.0
 * @package WordPress
 * @subpackage Sushi_WP
 */

require( LIB_ABSPATH . '/links.php' );
require( LIB_ABSPATH . '/core.php' );
require( LIB_ABSPATH . '/template.php' );

require( swp_assets_dir( 'classes/class.swp-media.php' ) );

$GLOBALS['sushiwp'] = array(
	'systeminfo'	=> null,
	'packages' 		=> null,
	'adminthemes'	=> null
);

/*
* END OF FILE
* load.php
*/
?>