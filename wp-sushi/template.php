<?php
/**
 * Sushi Worpdress Starter System Library
 *
 * General Template Functions
 *
 * @author Sushi Katana team
 * @copyright 2013 Sushi Digital Pty. Ltd.
 * @since Sashimi 3.0
 * @package WordPress
 * @subpackage Sushi_WP
 */

function swp_title()
{
	wp_title( '|', true, 'right' );
}

function _swp_title( $title, $sep )
{
	global $wp_query;
	
	if ( is_home() || is_front_page() )
		return get_bloginfo( 'name' );
	
	$pageobj = $wp_query->get_queried_object();
	if ( is_page() || is_single() )
		$pagetitle = $pageobj->post_title;
	else
		$pagetitle = get_bloginfo( 'name' );
		
	$title = sprintf( '%s | %s', $pagetitle, get_bloginfo( 'name' ) );
	
	return $title;
}
add_filter( 'wp_title', '_swp_title', 10, 2 );

function swp_head()
{
	// require jquery at all times.
	swp_load_js( 'jquery' );	
	// enqueue styles and scripts
	do_action( 'swp_load_frontend_scripts' );
	// enqueue styles and scripts that must go last at all times in their respective places.
	add_action( 'wp_enqueue_scripts', 'swp_load_late_scripts', 999 );			
	// call wp_head().
	wp_head();
	// print scripts
	do_action( 'swp_print_scripts' );
}

function swp_footer()
{
	// call wp_footer().
	wp_footer();
}

/**
 * Retrieves the associated featured image (post thumbnail).
 *
 * @since 3.0
 *
 * @param string $return           	Return types ( 'id', 'src', 'object', 'timthumbimg', 'timthumbsrc' ). Default is 'timthumbimg'.
 * @param int	 $post_id			ID of the post. If post id is not specified, it assumes it's inside the loop. Default is null.
 * @param array	 $tt_params         Timthumb parameters.
 * @param array	 $tt_attrs          Timthumb attributes.
 */
function get_featured_img( $return = 'timthumbimg', $post_id = null, $tt_params = array(), $tt_attrs = array() ) 
{
	if ( !current_theme_supports( 'post-thumbnails' ) ) {
		echo '<p>Sorry, your Wordpress setup doesn\'t seem to support post thumbnails. Please see add_theme_support() function.</p>';		
	}	
		
	// Ensure $return has a correct value.
	if ( ! in_array( $return, array( 'id', 'src', 'object', 'timthumbimg', 'timthumbsrc' ) ) )
		$return = 'timthumbimg';
		
	// Check if we are inside a wordpress loop and if the template has featured img.
	if ( in_the_loop() && !has_post_thumbnail() ) {	
		return false;
	}
	
	$post_id = ( $post_id === null ) ? get_the_ID() : $post_id;
	$thumb_id = get_post_meta( $post_id, '_thumbnail_id', true );
	
	$img = new SWP_Media( get_post( $post_id ) );		
	
	if ( $img ) {		
		
		$attrs = array_merge( array( 'src' => $img->url, 'title' => $img->title, 'alt' => $img->alt ), $tt_attrs );
		$params = array_merge( array( 'src' => $img->url, 'w' => $img->width, 'h' => $img->height ), $tt_params );
	
		switch ( $return ) {
			case 'id':
				return $img->ID;
			case 'src':
				return $img->url;
			case 'timthumbimg':				
				return get_timthumb_img( $attrs, $params );
			case 'timthumbsrc':
				return get_timthumb_src( $params );
			default:
				return $img;
		}
	}
	
	return false;
}

function get_timthumb_img( array $attributes, array $params )
{
	$params = array_merge( swp_timthumb_def_params(), $params );	
	foreach ( $attributes as $key => $val ) {
		$attrs[] = $key . '="' . $val . '"';		
	}
	return sprintf( '<img src="%s" %s />', get_timthumb_src( $params ), implode( " ", $attrs ) );
}

function get_timthumb_src( array $params )
{
	$params = array_merge( swp_timthumb_def_params(), $params );	
	foreach ( $params as $key => $val ) {
		if ( in_array( $key, swp_timthumb_default_params_list() ) ) {
			$query[] = $key . '=' . $val;
		}
	}
	return sprintf( '%s?%s', swp_timthumb_url(), @implode( '&', $query ) );
}

function swp_popular_posts( $limit = 5, $category_param = '', $category_values = '', $title_limit = 5 )
{	
	if ( ! empty( $category_param ) && ! empty( $category_values ) &&
		in_array( $category_param, array( 'cat', 'category_name', 'category__and', 'category__in', 'category__not_in' ) ) ) {
		
		if ( gettype( $category_values ) == 'string' ) {
			$category_values = trim( $category_values );
			$category = sprintf( '&%s=%s', $category_param, rtrim( $category_values, ',' ) );
		} else if (  gettype( $category_values ) == 'array' ) {
			$category = sprintf( '&%s=%s', $category_param, @implode( ',', $category_values ) );
		} else {
			$category = '';
		}
	} else {
		$category = '';
	}	
	
	$popular = new WP_Query( sprintf( 'orderby=comment_count&posts_per_page=%s%s', $limit, $category ) );
	if ( $popular->have_posts() ) : 
		while ( $popular->have_posts() ) : $popular->the_post();
?>
	<li id="<?php echo 'post-' . get_the_id(); ?>"><a href="<?php echo get_permalink(); ?>"><?php echo wp_trim_words( get_the_title(), $title_limit, '...' ); ?></a></li>
<?php
		endwhile;
		wp_reset_postdata();
	endif;
}

/*
* END OF FILE
* general.php
*/
?>