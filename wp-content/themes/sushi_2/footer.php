<?php
/**
 * Sushi Wordpress Starter 3.0 Theme footer.
 *
 * @author Sushi Katana team
 * @copyright 2013 Sushi Digital Pty. Ltd.
 * @since Sashimi 3.0
 * @package WordPress
 * @subpackage Sushi_WP
 */
?>
	<!--
	<footer class="row">
		<div class="main-container">
			<?php wp_nav_menu( array( "container" => false, 'depth' => 0 ) ); ?>
			<div class="clr"></div>
			<div class="copyright left">Copyright &copy; <?php echo date( 'Y' ); ?> <a href="<?php bloginfo( 'home' ); ?>" title="AirVision">Airvision</a>. All Rights Reserved. </div>
			<div class="site right"><a href="http://www.sushidigital.com.au/" target="_blank">Sushi Digital</a> Website Design Perth</div>
			<div class="clr"></div>
		</div>
	</footer>
	-->
<style>
.footercc ul li a{font-size:12px;}
</style>	
<footer class="row footercc">
		<div class="main-container">
		<?php wp_nav_menu( array( "container" => false, 'depth' => 0 ) ); ?>
		<div class="clr"></div>
			<div class="copyright left">Copyright &copy; <?php echo date( 'Y' ); ?> <a href="<?php bloginfo( 'home' ); ?>" title="AirVision">Airvision</a>. All Rights Reserved. </div>
			<div class="site right"><a href="http://www.sushidigital.com.au/" target="_blank">Sushi Digital</a> Website Design Perth</div>
			<div class="clr"></div>
		</div>
	</footer>
</div>
<!-- /main-wrapper -->
<script type="text/javascript" src="https://web.archive.org/web/20171023032637js_/https://www.airvision.com.au/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.51.0-2014.06.20"></script>
<script type="text/javascript">
/* <![CDATA[ */
var _wpcf7 = {"loaderUrl":"https:\/\/web.archive.org\/web\/20171023032637\/https:\/\/www.airvision.com.au\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","sending":"Sending ..."};
/* ]]> */
</script>
<script type="text/javascript" src="https://web.archive.org/web/20171023032637js_/https://www.airvision.com.au/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.2.2"></script>
<script type="text/javascript" src="https://web.archive.org/web/20171023032637js_/https://www.airvision.com.au/wp-content/themes/sushi_2/js/global.js?ver=3.0.1"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59830214-1', 'auto');
  ga('send', 'pageview');

</script>
</body>

</html>