<?php
/**
 * Template Name: Latest Updates
 *
 * @author Sushi Katana team
 * @copyright 2013 Sushi Digital Pty. Ltd.
 * @since Sashimi 3.0
 * @package WordPress
 * @subpackage Sushi_WP
 */
	get_header();
	
	$page_object = get_queried_object();
	$page_id  = get_queried_object_id(); // Get current page id
?>
	<section id="banner" class="contact row">
		<div class="main-container">
		<!--
			<?php 
				if( have_posts() ) :
					while( have_posts() ) : the_post();
			?>
						<?php 
							if( get_field( 'page_banner_text', $page_id ) ) :
								while( has_sub_field( 'page_banner_text', $page_id ) ) :
						?>
									<div class="text-overlay">
										<h2><?php echo get_sub_field( 'title' ); ?></h2>
										<?php echo apply_filters( 'the_content', get_sub_field( 'content' ) ); ?>
									</div>
						<?php
								endwhile;
							endif;
						?>
						<?php if( has_post_thumbnail() ) :  ?>
							<div class="img-holder">
								<?php
									$img_id = get_post_meta( get_the_ID(), '_thumbnail_id', true );
									$alt_text = get_post_meta( $img_id , '_wp_attachment_image_alt', true);
									$image_url = wp_get_attachment_image_src( $img_id, 'full' );
									echo get_timthumb_img( array( 'alt' => $alt_text, 'title' => $alt_text ), array( 'src' => $image_url[0], 'w' => 1136, 'h' => 338 ) );
								?>
							</div>
						<?php endif; ?>
			<?php
					endwhile;
					wp_reset_query();
				endif; 
			?>
		-->	
		<div class="text-overlay">
		<?php echo get_field('latest_updates_title'); ?>
		</div>	
		<style>
		.img-holder img{
		margin-bottom:-15px !important;
		}
		</style>		
		<div class="img-holder">
		<?php echo get_field('latest_updates_banner'); ?>
		</div>				
		</div>
	</section>
	<section id="content-area" class="latest-updates row">
		<div class="main-container">
			<div class="main-content">
				<?php 
					if( have_posts() ) :
						while( have_posts() ) : the_post();
							the_title( '<h1>', '</h1>' );
							the_content();
							//echo apply_filters( 'the_content', get_field( 'contact_content', $page_id ) );
						endwhile;
					endif;
				?>
				
				<div class="latest-updates-section">
					<div id="updates">
						<div class="updates-wrapper">
							<ul class="news-lists">
								<?php
									$paged = get_query_var('paged') ? get_query_var('paged') : 1;
									$args  = array(
										'cat'			 => 4,
										'order'			 => 'DESC',
										'orderby'		 => 'date',
										'post_type'		 => 'post',
										'post_status'	 => 'publish',
										'posts_per_page' => 6, //-1,
										'paged'          => $paged
									);
									$updates = new WP_Query( $args );
									$ctr     = 0;
									if( $updates->have_posts() ) :
										while( $updates->have_posts() ) : $updates->the_post(); $ctr++;
								?>
								<li <?php echo ( $ctr%2 == 0 )? 'class="no-mr"':''; ?>>
									<div class="news-content">
										<div class="news-featimg">
											<?php if( has_post_thumbnail() ) : ?>
												<a href="<?php the_permalink(); ?>">
													<div class="img-holder">
														<?php
															$timthumb_ZC = get_field('timthumb_zoom_crop', get_the_ID() );
															$img_id = get_post_meta( get_the_ID(), '_thumbnail_id', true );
															$alt_text = get_post_meta( $img_id , '_wp_attachment_image_alt', true);
															$image_url = wp_get_attachment_image_src( $img_id, 'full' );
															//echo get_timthumb_img( array( 'alt' => $alt_text, 'title' => $alt_text ), array( 'src' => $image_url[0], 'w' => 490, 'h' => 243, 'zc' => $timthumb_ZC ) );
															the_post_thumbnail( array( 490, 243 ) );
														?>
													</div>
												</a>
											<?php else: ?>
												<a href="<?php the_permalink(); ?>">
													<div class="img-holder default">
														<img src="<?php echo get_bloginfo('template_url') ?>/images/AirVision-Logo.png" alt="AirVision" title="AirVision">
													</div>
												</a>	
													
											<?php endif; ?>
										</div>
										<div class="news_title">
											<h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
											<div class="date"><?php the_time( 'M d, Y' ); ?></div>
										</div>
										
										<div class="content-text">
											<p><?php echo limit_words(get_the_content(), 45); ?></p>
										</div>

										<a href="<?php echo get_permalink( $post->ID ); ?>" class="read-more">Read More</a>
										
										<div class="clr"></div>
									</div>
								</li>
								<?php   endwhile; wp_reset_postdata();
									endif; ?>
								
							</ul>
							<div class="pagination bottom container">
								<?php	$paging = array(
											'base'         => @add_query_arg('paged','%#%'),
											'format'       => '?paged=%#%',
											'current'      => max( 1, get_query_var('paged') ),
											'prev_text'    => __('First'),
											'next_text'    => __('Last'),
											'total'        => $updates->max_num_pages
										);
										//display pagination		
										echo paginate_links( $paging );
								?>
							</div>
						</div>	
					</div>
					<div class="clr"></div>
				</div>

			</div>

			<?php get_template_part( 'content-here-to-help' ); ?>	
		</div>
	</section>
	<?php 
		get_template_part( 'content-bottom-area' );
	?>
<?php get_footer(); ?>