<div class="buildings">
	<?php 
		if( get_field( 'about_us_buildings', $page_id ) ) :
			while( has_sub_field( 'about_us_buildings', $page_id ) ) :
				$image = get_sub_field( 'image' );
	?>
				<div class="bldg-entry entry-<?php echo get_sub_field( 'order' ); if( !$image ) { echo ' ' . 'no-image'; }?>">
					<div class="content-text">
						<h2><?php echo get_sub_field( 'title' ); ?></h2>
						<?php echo apply_filters( 'the_content', get_sub_field( 'content' ) ); ?>
					</div>
					<?php if( $image ) : ?>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" title="<?php echo $image['alt']; ?>" />
					<?php endif; ?>
				</div>
	<?php
			endwhile;
		endif;
	?>
	<div class="clr"></div>
</div>