<?php
/**
 * Sushi Wordpress Starter 3.0 Theme Search Results pages.
 *
 * @author Sushi Katana team
 * @copyright 2013 Sushi Digital Pty. Ltd.
 * @since Sashimi 3.0
 * @package WordPress
 * @subpackage Sushi_WP
 */
	get_header();
?>
	<section id="content-area" class="row">
		<div class="main-container">
			<div class="main-content search">
				<h1 class="search"><?php printf( __( 'Search Results for: %s', 'sushi' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
				<?php
					if( have_posts() ) :
						while( have_posts() ) : the_post();
				?>
							<div class="post-entry">
				<?php
							if( has_post_thumbnail() ) :
				?>
								<div class="img-holder left">
									<?php
										$img_id = get_post_meta( get_the_ID(), '_thumbnail_id', true );
										$alt_text = get_post_meta( $img_id , '_wp_attachment_image_alt', true);
										$image_url = wp_get_attachment_image_src( $img_id, 'full' );
										echo get_timthumb_img( array( 'alt' => $alt_text, 'title' => $alt_text ), array( 'src' => $image_url[0], 'w' => 330, 'h' => 330 ) );
									?>
								</div>
				<?php
							endif;
								the_content();
				?>
							</div>
				<?php
						endwhile;
					else :
				?>
						<h1 class="search"><?php _e( 'Nothing Found', 'sushi' ); ?></h1>
						<p><?php _e('Sorry, but nothing matched your search criteria. Please try again with some different keywords.'); ?></p>
				<?php
					endif;
				?>
			</div>
			<div class="clr"></div>
			<?php get_template_part( 'content-here-to-help' ); ?>
		</div>
	</section>
	<?php get_template_part( 'content-bottom-area' ); ?>
<?php get_footer(); ?>