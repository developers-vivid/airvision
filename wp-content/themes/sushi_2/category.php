<?php
/**
 * Sushi Wordpress Starter 3.0 Theme category template.
 *
 * @author Sushi Katana team
 * @copyright 2013 Sushi Digital Pty. Ltd.
 * @since Sashimi 3.0
 * @package WordPress
 * @subpackage Sushi_WP
 */
get_header();
?>

<?php get_footer(); ?>