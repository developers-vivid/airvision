<?php
/**
 * Sushi Wordpress Starter 3.0 Theme 404 template.
 *
 * @author Sushi Katana team
 * @copyright 2013 Sushi Digital Pty. Ltd.
 * @since Sashimi 3.0
 * @package WordPress
 * @subpackage Sushi_WP
 */
	get_header();
?>
	<section id="content-area" class="row">
		<div class="main-container">
			<div class="main-content error404">
				<h1>Error 404 - Page Not Found</h1>
				<p>Sorry we were unable to find what you tried looking for.</p>
			</div>
			<div class="clr"></div>
			<?php get_template_part( 'content-here-to-help' ); ?>
		</div>
	</section>
	<?php get_template_part( 'content-bottom-area' ); ?>
<?php get_footer(); ?>