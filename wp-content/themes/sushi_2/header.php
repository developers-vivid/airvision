<!DOCTYPE html>
<html lang="en-US">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head><script src="../../../../../archive.org/includes/analytics2a5b.js?v=cf34f82" type="text/javascript"></script>
<script type="text/javascript">window.addEventListener('DOMContentLoaded',function(){var v=archive_analytics.values;v.service='wb';v.server_name='wwwb-app16.us.archive.org';v.server_ms=2246;archive_analytics.send_pageview({});});</script><script type="text/javascript" src="https://web.archive.org/static/js/wbhack.js?v=1522452177.0" charset="utf-8"></script>

<link rel="stylesheet" type="text/css" href="https://web.archive.org/static/css/banner-styles.css?v=1522452177.0" />
<link rel="stylesheet" type="text/css" href="https://web.archive.org/static/css/iconochive.css?v=1522452177.0" />

<!-- End Wayback Rewrite JS Include -->
<title>Airvision | Central Plant Solutions</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=1156"/>
<script src="https://web.archive.org/web/20171023032637js_/http://code.jquery.com/jquery-2.1.4.min.js"></script>

<!-- This site is optimized with the Yoast SEO plugin v2.3.2 - https://yoast.com/wordpress/plugins/seo/ -->
<meta name="description" content="Airvision are the leaders in Perth for Chillers &amp; Central Plant Solutions."/>
<meta name="keywords" content="industrial air conditioning air conditioning air conditioning perth chillers Trane Powerpax Airvision are the leaders in Perth for Chillers &amp; Central Plant Solutions"/>
<link rel="canonical" href="https://web.archive.org/web/20171023032637/https://www.airvision.com.au/"/>
<meta property="og:locale" content="en_US"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="Airvision | Central Plant Solutions"/>
<meta property="og:description" content="Airvision are the leaders in Perth for Chillers &amp; Central Plant Solutions."/>
<meta property="og:url" content="https://web.archive.org/web/20171023032637/https://www.airvision.com.au/"/>
<meta property="og:site_name" content="Airvision – Central Plant Solutions"/>
<script type="application/ld+json">{"@context":"https:\/\/web.archive.org\/web\/20171023032637\/http:\/\/schema.org\/","@type":"WebSite","url":"https:\/\/web.archive.org\/web\/20171023032637\/https:\/\/www.airvision.com.au\/","name":"Airvision \u2013 Central Plant Solutions","potentialAction":{"@type":"SearchAction","target":"https:\/\/web.archive.org\/web\/20171023032637\/https:\/\/www.airvision.com.au\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
<meta name="msvalidate.01" content="A092F27644933C729C2C68B09ADDD909"/>
<meta name="google-site-verification" content="URW0xCBQBOQK5CzWYcCkOkt3SIE9j_QrNI4qyRp3qmU"/>
<!-- / Yoast SEO plugin. -->

<link rel="alternate" type="application/rss+xml" title="Airvision – Central Plant Solutions » Home Comments Feed" href="https://web.archive.org/web/20171023032637/https://www.airvision.com.au/home/feed/"/>
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/web.archive.org\/web\/20171023032637\/https:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"https:\/\/web.archive.org\/web\/20171023032637\/https:\/\/www.airvision.com.au\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.3.1"}};
			!function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel="stylesheet" id="contact-form-7-css" href="https://web.archive.org/web/20171023032637cs_/https://www.airvision.com.au/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.2.2" type="text/css" media="all"/>
<link rel="stylesheet" id="global-stylesheet-css" href="https://web.archive.org/web/20171023032637cs_/https://www.airvision.com.au/wp-content/themes/sushi_2/css/global.css?ver=3.0.1" type="text/css" media="all"/>
<script type="text/javascript" src="https://web.archive.org/web/20171023032637js_/https://www.airvision.com.au/wp-includes/js/jquery/jquery.js?ver=1.11.3"></script>
<script type="text/javascript" src="https://web.archive.org/web/20171023032637js_/https://www.airvision.com.au/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1"></script>
<script type="text/javascript" src="https://web.archive.org/web/20171023032637js_/https://www.airvision.com.au/wp-sushi/assets/scripts/jquery.sushi.min.js?ver=3.0.1"></script>
<script type="text/javascript" src="https://web.archive.org/web/20171023032637js_/https://www.airvision.com.au/wp-content/themes/sushi_2/js/jquery-plugins/easypaginate.js?ver=3.0.1"></script>
<script type="text/javascript" src="https://web.archive.org/web/20171023032637js_/https://www.airvision.com.au/wp-content/themes/sushi_2/js/jquery-plugins/jquery.fancybox.js?ver=3.0.1"></script>
<script type="text/javascript" src="https://web.archive.org/web/20171023032637js_/https://www.airvision.com.au/wp-content/themes/sushi_2/js/jquery-plugins/jquery.bxslider.js?ver=3.0.1"></script>
<script type="text/javascript" src="https://web.archive.org/web/20171023032637js_/https://www.airvision.com.au/wp-content/plugins/flash-album-gallery/admin/js/swfobject.js?ver=2.2"></script>
<script type="text/javascript" src="https://web.archive.org/web/20171023032637js_/https://www.airvision.com.au/wp-content/plugins/flash-album-gallery/admin/js/swfaddress.js?ver=2.4"></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.airvision.com.au/xmlrpc.php?rsd"/>
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://www.airvision.com.au/wp-includes/wlwmanifest.xml"/> 
<meta name="generator" content="WordPress 4.3.1"/>
<link rel="shortlink" href="https://web.archive.org/web/20171023032637/https://www.airvision.com.au/"/>

<!-- Bad Behavior 2.2.16 run time: 4.372 ms -->
<script type="text/javascript">
<!--
function bb2_addLoadEvent(func) {
	var oldonload = window.onload;
	if (typeof window.onload != 'function') {
		window.onload = func;
	} else {
		window.onload = function() {
			oldonload();
			func();
		}
	}
}

bb2_addLoadEvent(function() {
	for ( i=0; i < document.forms.length; i++ ) {
		if (document.forms[i].method == 'post') {
			var myElement = document.createElement('input');
			myElement.setAttribute('type', 'hidden');
			myElement.name = 'bb2_screener_';
			myElement.value = '1508729600 207.241.229.235';
			document.forms[i].appendChild(myElement);
		}
	}
});
// --></script>
		
<!-- <meta name='Grand Flagallery' content='4.35' /> -->
	<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
<link rel="shortcut icon" type="image/x-icon" href="https://web.archive.org/web/20171023032637im_/https://www.airvision.com.au/wp-content/themes/sushi_2/favicon.ico?t=1508729601"/>
<!--[if lt IE 9]>
<script src="https://www.airvision.com.au/wp-sushi/assets/scripts/html5shiv.js"></script>
<style type="text/css">
	* { *behavior: url( "https://www.airvision.com.au/wp-sushi/assets/scripts/boxsizing.htc" ); }
</style>
<![endif]-->
</head>
<body class="home page page-id-5 page-template page-template-page-home page-template-page-home-php front"><!-- BEGIN WAYBACK TOOLBAR INSERT -->
<script type="text/javascript" src="https://web.archive.org/static/js/timestamp.js?v=1522452177.0" charset="utf-8"></script>
<script type="text/javascript" src="https://web.archive.org/static/js/graph-calc.js?v=1522452177.0" charset="utf-8"></script>
<script type="text/javascript" src="https://web.archive.org/static/js/auto-complete.js?v=1522452177.0" charset="utf-8"></script>
<script type="text/javascript" src="https://web.archive.org/static/js/toolbar.js?v=1522452177.0" charset="utf-8"></script>

<style type="text/css">
body {
  margin-top:0 !important;
  padding-top:0 !important;
  /*min-width:800px !important;*/
}
.wb-autocomplete-suggestions {
    text-align: left; cursor: default; border: 1px solid #ccc; border-top: 0; background: #fff; box-shadow: -1px 1px 3px rgba(0,0,0,.1);
    position: absolute; display: none; z-index: 2147483647; max-height: 254px; overflow: hidden; overflow-y: auto; box-sizing: border-box;
}
.wb-autocomplete-suggestion { position: relative; padding: 0 .6em; line-height: 23px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; font-size: 1.02em; color: #333; }
.wb-autocomplete-suggestion b { font-weight: bold; }
.wb-autocomplete-suggestion.selected { background: #f0f0f0; }
</style>
<div id="main-wrapper">
	<header class="row">
		<div class="main-container">
			<div id="logo" class="left">
				<a href="<?php echo get_home_url(); ?>" title="AirVision Logo">
					<img src="https://web.archive.org/web/20171023032637im_/https://www.airvision.com.au/wp-content/themes/sushi_2/images/Air-Vision-Logo.png" alt="Airvision – Central Plant Solutions" title="Airvision – Central Plant Solutions"/>
				</a>
			</div>
			<nav class="row right">
			<?php wp_nav_menu( array( "container" => false ) ); ?>
			</nav>
			<!--<div id="search" class="row right">
				<img src="/images/search-icon.png" alt="Search" />
								<div class="clr"></div>
			</div>-->
			<div class="clr"></div>
		</div>
	</header>