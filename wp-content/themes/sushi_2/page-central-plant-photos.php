<?php
/**
 * Template Name: Central Plant Photos
 *
 * @author Sushi Katana team
 * @copyright 2013 Sushi Digital Pty. Ltd.
 * @since Sashimi 3.0
 * @package WordPress
 * @subpackage Sushi_WP
 */
	get_header();
	
	$page_object = get_queried_object();
	$page_id  = get_queried_object_id(); // Get current page id
?>
	<section id="banner" class="row">
		<div class="main-container">
								<div class="text-overlay">
								<?php echo get_field('banner-title'); ?>
								</div>
								<style>
								.img-holder img{
									margin-bottom:-15px !important;
								}
								</style>
								<div class="img-holder">
								<?php echo get_field('banner-image'); ?>
								</div>				
		</div>	
		<div class="main-container">
		<?php 
			if( have_posts() ) :
				while( have_posts() ) : the_post();
		?>
					<?php 
						if( get_field( 'page_banner_text', $page_id ) ) :
							while( has_sub_field( 'page_banner_text', $page_id ) ) :
					?>
								<div class="text-overlay">
									<h2><?php echo get_sub_field( 'title' ); ?></h2>
									<?php echo apply_filters( 'the_content', get_sub_field( 'content' ) ); ?>
								</div>
					<?php
							endwhile;
						endif;
					?>
					<?php if( has_post_thumbnail() ) :  ?>
						<div class="img-holder">
							<?php
								$img_id = get_post_meta( get_the_ID(), '_thumbnail_id', true );
								$alt_text = get_post_meta( $img_id , '_wp_attachment_image_alt', true);
								$image_url = wp_get_attachment_image_src( $img_id, 'full' );
								echo get_timthumb_img( array( 'alt' => $alt_text, 'title' => $alt_text ), array( 'src' => $image_url[0], 'w' => 1136, 'h' => 338 ) );
							?>
						</div>
					<?php endif; ?>
		<?php
				endwhile;
				wp_reset_query();
			endif; 
		?>
		</div>
	</section>
	<section id="content-area" class="cpp row">
		<div class="main-container">
			<div class="main-content">
			<h1><?php the_title(); ?></h1>
			<div class="clr"></div>
			<?php
			// TO SHOW THE PAGE CONTENTS
			while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
			<div class="entry-content-page">
            <?php the_content(); ?> <!-- Page Content -->
			</div><!-- .entry-content-page -->
			<?php
			endwhile; //resetting the page loop
			wp_reset_query(); //resetting the page query
			?>					
				<div class="clr"></div>
				<div class="image-post">
					<ul id="gal-images">
				<?php echo get_field('photos'); ?>	
					<!--
					<?php if( get_field( 'photos', $page_id ) ) : ?>
						<?php while( has_sub_field( 'photos', $page_id )):
									$img = get_sub_field( 'photo_image' );
									if( $img ) :
										$attributes = array( "alt"=> $img['title'] , "title"=> $img['title'] );
										$params = array('src'=> $img['url'] , 'w'=>'330', 'h'=>'330', 'zc'=> '1');
						?>
							<li data-fancybox-group="gallery" class="fancybox" rel="group" href="<?php echo $img['url']; ?>">
								<?php echo get_timthumb_img($attributes,$params); ?>
							</li>
						<?php  
									endif;
								endwhile; 
							 endif;
						?>
					-->	
					</ul>
					<div class="clr"></div>
				</div>
				<div class="clr"></div>
			</div>
			<?php 
				get_template_part( 'content-here-to-help' );
			?>
		</div>
	</section>
	<?php get_template_part( 'content-bottom-area' ); ?>
<?php get_footer(); ?>