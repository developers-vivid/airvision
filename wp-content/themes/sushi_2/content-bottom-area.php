<section id="bottom-area">
	<div class="main-container">

		<div class="latest-updates left">
			<a href="<?php echo get_home_url(); ?>/latest-updates"><h2><?php echo get_cat_name( 4 ); ?></h2></a>
			<div class="clr"></div>
			<div class="line"></div>
			<div id="updates">
				<div class="updates-wrapper">
					<ul class="bxslider">
					<?php
						$args = array(
							'cat'	=> 4,
							'order'	=> 'DESC',
							'orderby'	=> 'date',
							'post_type'		=> 'post',
							'post_status'	=> 'publish',
							'posts_per_page'	=> -1
						);
						$updates = new WP_Query( $args );
						if( $updates->have_posts() ) :
							while( $updates->have_posts() ) : $updates->the_post();
					?>
						<li>
							<div class="slide slides">
								<h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
								<div class="date"><?php the_time( 'M d' ); ?></div>
								<div class="clr"></div>
								<div class="content-text">
									<p><?php echo limit_words(get_the_content(), 16); ?></p>
									<?php if( has_post_thumbnail() ) : ?>
										<a href="<?php the_permalink(); ?>">
											<div class="img-holder">
												<?php
												the_post_thumbnail( array( 419, 175 ) );												
												/*
													$img_id = get_post_meta( get_the_ID(), '_thumbnail_id', true );
													$alt_text = get_post_meta( $img_id , '_wp_attachment_image_alt', true);
													$image_url = wp_get_attachment_image_src( $img_id, 'full' );
													echo get_timthumb_img( array( 'alt' => $alt_text, 'title' => $alt_text ), array( 'src' => $image_url[0], 'w' => 419, 'h' => 175 ) );
												*/
												?>
											</div>
										</a>
									<?php else: ?>
										<a href="<?php the_permalink(); ?>">
											<div class="img-holder default">
												<img src="<?php echo get_bloginfo('template_url') ?>/images/Air-Vision-Logo.png" alt="AirVision" title="AirVision">
											</div>
										</a>	
									<?php endif; ?>
								</div>
							</div>
						</li>
					<?php
							endwhile;
						endif;
					?>
					</ul>
				</div>	
				<span id="prev2"></span>
				<span id="next2"></span>
			</div>
			<div class="clr"></div>
		</div>

		<div class="contact right">
			<h2><?php echo get_field( 'contact_info_header', 139); ?></h2>
			<div class="line"></div>
			<div class="content-text">
				<?php //echo apply_filters( 'the_content', get_field( 'contact_info_content', 5 ) ); ?>	
				<?php echo ( get_field( 'number_contact', 139 ) )? '<p><strong class="ph-fax">Phone:</strong> '.get_field( 'number_contact', 139 ).'</p>' : ''; ?>
				<?php echo ( get_field( 'facsimile', 139 ) )? '<p><strong class="ph-fax">Fax:</strong> '.get_field( 'facsimile', 139 ).'</p>' : ''; ?>				
				<?php echo ( get_field( 'address_contact', 139 ) )? '<p class="contact-addr">'.get_field( 'address_contact', 139 ).'</p>' : ''; ?>
				<?php echo ( get_field( 'contact_email', 139 ) )? '<p><a href="mailto:'.get_field( 'contact_email', 139 ).'">'.get_field( 'contact_email', 139 ).'</a></p>' : ''; ?>
			</div>
		</div>
		
		<div class="clr"></div>
	</div>
</section>