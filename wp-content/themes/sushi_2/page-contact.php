<?php
/**
 * Template Name: contact us
 *
 * @author Sushi Katana team
 * @copyright 2013 Sushi Digital Pty. Ltd.
 * @since Sashimi 3.0
 * @package WordPress
 * @subpackage Sushi_WP
 */
	get_header();
	
	$page_object = get_queried_object();
	$page_id  = get_queried_object_id(); // Get current page id
?>
	<section id="banner" class="contact row">
		<div class="main-container">
								<div class="text-overlay">
								<?php echo get_field('banner-title'); ?>
								</div>
								<style>
								.img-holder img{
									margin-bottom:-15px !important;
								}
								</style>
								<div class="img-holder">
								<?php echo get_field('banner-image'); ?>
								</div>			
		</div>
		<div class="main-container">
			<?php 
				if( have_posts() ) :
					while( have_posts() ) : the_post();
			?>
						<?php 
							if( get_field( 'page_banner_text', $page_id ) ) :
								while( has_sub_field( 'page_banner_text', $page_id ) ) :
						?>
									<div class="text-overlay">
										<h2><?php echo get_sub_field( 'title' ); ?></h2>
										<?php echo apply_filters( 'the_content', get_sub_field( 'content' ) ); ?>
									</div>
						<?php
								endwhile;
							endif;
						?>
						<?php if( has_post_thumbnail() ) :  ?>
							<div class="img-holder">
								<?php
									$img_id = get_post_meta( get_the_ID(), '_thumbnail_id', true );
									$alt_text = get_post_meta( $img_id , '_wp_attachment_image_alt', true);
									$image_url = wp_get_attachment_image_src( $img_id, 'full' );
									echo get_timthumb_img( array( 'alt' => $alt_text, 'title' => $alt_text ), array( 'src' => $image_url[0], 'w' => 1136, 'h' => 338 ) );
								?>
							</div>
						<?php endif; ?>
			<?php
					endwhile;
					wp_reset_query();
				endif; 
			?>
		</div>
	</section>
	<section id="content-area" class="contact row">
		<div class="main-container">
			<div class="main-content">
				<?php 
					if( have_posts() ) :
						while( have_posts() ) : the_post();
							the_title( '<h1>', '</h1>' );
							echo apply_filters( 'the_content', get_field( 'contact_content', $page_id ) );
						endwhile;
					endif;
				?>
				<div class="map-area">
					<div class="map left">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3386.9144090331906!2d115.81760921481323!3d-31.908922827439724!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2a32af91acff4d8d%3A0xe7c51f06c67a2d8c!2s6%2F52+Frobisher+St%2C+Osborne+Park+WA+6017%2C+Australia!5e0!3m2!1sen!2sph!4v1522997587568" width="530" height="387" frameborder="0" style="border:0" allowfullscreen></iframe>
						<!--
						<a href="<?php echo get_field("contact_map_link", $page_id); ?>" target="_blank">
							<img src="<?php echo get_field("contact_map", $page_id); ?>" title="6/52 Frobisher Street  Osborne Park  Western Australia 6017" alt="6/52 Frobisher Street  Osborne Park  Western Australia 6017" width="530" height="387">
						</a>
						-->
					</div>
					<div class="details-contact right">
						<div class="contact">
							<div class="contact-entry">
								<div class="compadd"></div>
								<div class="content-text compadd-cont">
									<h3>Office:</h3>
									<p><?php echo get_field("address_contact", $page_id); ?></p>
								</div>
								<div class="clr"></div>
							</div>
							<div class="contact-entry">
								<div class="compnum"></div>		
								<div class="content-text compnum-cont">
									<h3>Phone:</h3>
									<p><?php echo get_field("number_contact", $page_id); ?></p>
								</div>
								<div class="clr"></div>
							</div>
							<div class="contact-entry">
								<div class="facsi"></div>	
								<div class="content-text facsi-cont">
									<h3>Fax:</h3>
									<p><?php echo get_field("facsimile", $page_id); ?></p>
								</div>
								<div class="clr"></div>
							</div>
							<div class="contact-entry">
								<div class="comppos"></div>	
								<div class="content-text comppos-cont">
									<h3>Post:</h3>
									<p><?php echo get_field("postal_address", $page_id); ?></p>
								</div>
								<div class="clr"></div>
							</div>
						</div>					 		
					</div>
					<div class="clr"></div>
				 </div>
				<div id="admin-container">
					<div class="adminpost fL">
						<h2>Administration</h2>
						<ul class="clearfix">
							<!--
							<?php if(get_field('admin', $page_id) ): ?>
								<?php while(has_sub_field("admin", $page_id)):?>
								<li>	
									<!--<h4><?php echo get_sub_field("admin_name"); ?></h4>-->
									<a href="mailto:<?php echo get_sub_field("admin_email"); ?>"><?php echo get_sub_field("admin_email"); ?></a>
								</li>
								<?php ;endwhile; ?>
							<?php endif; ?>
							-->
							<a href="mailto:<?php echo get_field("administration_email", $page_id); ?>"><?php echo get_field("administration_email", $page_id); ?></a>
						</ul>
					 </div>
					<div class="servicepost fL">
						<h2>Service</h2>
						<ul>
							<!--
							<?php if(get_field('service', $page_id) ): ?>
								<?php while(has_sub_field("service", $page_id)):?>
								<li>	
									<!--<h4><?php echo get_sub_field("service_name"); ?></h4>	-->				 			
									<a href="mailto:<?php echo get_sub_field("service_email"); ?>"><?php echo get_sub_field("service_email"); ?></a>
								</li>
								<?php endwhile; ?>
							<?php endif; ?>
							-->
							<a href="mailto:<?php echo get_field("service_email", $page_id); ?>"><?php echo get_field("service_email", $page_id); ?></a>
						</ul>
					</div>
					<div class="furtherpost fR">
						<h2>Further Information</h2>
						<ul>
							<!--
							<?php if(get_field('futher_information', $page_id) ): ?>
								<?php while(has_sub_field("futher_information", $page_id)):?>
								<li>	
									<!--<h4><?php echo get_sub_field("further_information_name"); ?></h4>	-->				 			
									<a href="mailto:<?php echo get_sub_field("further_information_email"); ?>"><?php echo get_sub_field("further_information_email"); ?></a>
								</li>
								<?php endwhile; ?>
							<?php endif; ?>
							-->
							<a href="mailto:<?php echo get_field("futher_information_email", $page_id); ?>"><?php echo get_field("futher_information_email", $page_id); ?></a>
						</ul>
					</div>
					<div class="clr"></div>
				</div>
			</div>
			<?php get_template_part( 'content-here-to-help' ); ?>
		</div>
	</section>
	<?php 
		get_template_part( 'content-bottom-area' );
	?>
<?php get_footer(); ?>