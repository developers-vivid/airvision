<div class="need-help">
	<h3><?php echo get_field( 'here_to_help_title', 143 ); ?></h3>
	<div class="line"></div>
	<br>
	<div class="content-wrapper">
		<?php
			if( get_field( 'here_to_help_repeater', 143 ) ) :
				while( has_sub_field( 'here_to_help_repeater', 143 ) ) :
		?>
				<div class="content-text">
					<h4><?php echo get_sub_field( 'title_hh', 143 ); ?></h4>
					<div class="line"></div>
					<p><?php echo get_sub_field( 'the_content_hh', 143 ); ?><p>
				</div>
		<?php
				endwhile;
			endif;
		?>
		<div class="clr"></div>
	</div>
</div>