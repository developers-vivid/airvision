<?php
/**
 * Sushi Wordpress Starter 3.0 Theme functions and definitions.
 *
 * @author Sushi Katana team
 * @copyright 2013 Sushi Digital Pty. Ltd.
 * @since Sashimi 3.0
 * @package WordPress
 * @subpackage Sushi_WP
 */

/* 
// error reporting in demo
ini_set( 'display_errors', true );
error_reporting( E_ALL ); 
*/

add_theme_support( 'post-thumbnails' );

if ( SWP_STARTER_MODE == 'local' ) {
	error_reporting( E_ALL );
}

if ( !defined( 'THEME_URL' ) ) 
	define( 'THEME_URL', get_template_directory_uri() );

function after_theme_setup() 
{
	show_admin_bar( false );
}
add_action( 'swp_after_theme_setup', 'after_theme_setup' );

/**
 * Additional styles and scripts should be loaded here. Must be hooked in 'swp_load_frontend_scripts' action. You can also dequeue scripts here.
 *
 * Use functions:
 *	- swp_load_css( 'unique-id', 'url', array( 'dependencies' ) | false, (media type) 'all' | 'screen' | 'print' and so on.., (renew) true | false  );  
 *	- swp_load_js( 'unique-id', 'url', array( 'dependencies' ) | false, (in footer?) true | false, (renew) true | false ); 
 *
 * Notes:
 *	- Dependencies (optional) are Array of the unique id's of all the registered styles/scripts that this style/script depends on. Set to false if none. Default is false.
 *	- Media type (optional) is the value of media attribute in the link. Default is 'all'.
 *	- In footer (optional) indicates whether the script should be placed in <head> or at the bottom before </body>. Default is false.
 *	- Renew (optional) indicates if the style/script must be unregistered if it is already registered. Default is false.
 *
 * @since 3.0
 */
function sushi_load_scripts()
{
	// Remove scripts from the list
	wp_dequeue_script( 'swfobject' );
	wp_dequeue_script( 'swfaddress' );
	
	// Add or load scripts
	swp_addnload_js( 'easypaginate', THEME_URL . '/js/jquery-plugins/easypaginate.js', array( 'jquery' ) );
	swp_addnload_js( 'fancybox', THEME_URL . '/js/jquery-plugins/jquery.fancybox.js', array( 'jquery' ) );
	swp_addnload_js( 'bxslider', THEME_URL . '/js/jquery-plugins/jquery.bxslider.js', array( 'jquery' ) );
	//swp_addnload_js( 'jcycle', THEME_URL . '/js/jquery-plugins/jquery.cycle.all.js', array( 'jquery' ) );
	
}
add_action( 'swp_load_frontend_scripts', 'sushi_load_scripts', 12 );

/* add_action( 'init', 'add_excerpts_to_pages' );
function add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
} */

/**
 * Adds two classes to the array of body classes.
 * The first is if the site has only had one author with published posts.
 * The second is if a singular post being displayed
 *
 * @since 1.0
 *
 */
function sushi_body_class( $classes )
{	
	if ( is_front_page() || is_home() ) {
		$classes[] = 'front';
	}

	return $classes;
}
add_filter( 'body_class', 'sushi_body_class' );

function limit_words($string, $word_limit) {
    $words = explode(" ",$string);
    return implode(" ",array_splice($words,0,$word_limit));
}
?>