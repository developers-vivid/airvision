/* 
* GLOBAL TOOLS AND UTILITIES SCRIPTS
* Place all custom js/jquery scripts here.
*
*/

/* Initialize and/or execute, code jQuery Scripts here */
jQuery( function($) {
	
	/* Initialize plugins to execute */
	$( document ).SushiPlugins({
		placeholder: true
	});
	
	$( '.plant-solutions-provider' ).hover(function(){
		//$( this ).find( '.content-text, .bg-overlay' ).stop().fadeOut();
		$( this ).find( '.content-text, .bg-overlay' ).stop().fadeTo( "slow", 0.45 );
	},
	function(){
		//$( this ).find( '.content-text, .bg-overlay' ).stop().fadeIn('slow');
		$( this ).find( '.content-text, .bg-overlay' ).stop().fadeTo( "slow", 1 );
    });
	
	$( '.plant-opti' ).hover(function(){
		//$( this ).find( '.content-text, .bg-overlay' ).stop().fadeOut();
		$( this ).find( '.content-text, .bg-overlay' ).stop().fadeTo( "slow", 0.45 );
	},
	function(){
		//$( this ).find( '.content-text, .bg-overlay' ).stop().fadeIn('slow');
		$( this ).find( '.content-text, .bg-overlay' ).stop().fadeTo( "slow", 1 );
	});
	
	$( '.repairs' ).hover(function(){
		//$( this ).find( '.content-text, .bg-overlay' ).stop().fadeOut();
		$( this ).find( '.content-text, .bg-overlay' ).stop().fadeTo( "slow", 0.45 );
	},
	function(){
		//$( this ).find( '.content-text, .bg-overlay' ).stop().fadeIn('slow');
		$( this ).find( '.content-text, .bg-overlay' ).stop().fadeTo( "slow", 1 );
	});
	
	$( '.aquisition' ).hover(function(){
		//$( this ).find( '.content-text, .bg-overlay' ).stop().fadeOut();
		$( this ).find( '.content-text, .bg-overlay' ).stop().fadeTo( "slow", 0.45 );
	},
	function(){
		//$( this ).find( '.content-text, .bg-overlay' ).stop().fadeIn('slow');
		$( this ).find( '.content-text, .bg-overlay' ).stop().fadeTo( "slow", 1 );
	});
	
	$( '.medium-level-bldg' ).hover(function(){
		//$( this ).find( '.content-text, .bg-overlay' ).stop().fadeOut();
		$( this ).find( '.content-text, .bg-overlay' ).stop().fadeTo( "slow", 0.45 );
	},
	function(){
		//$( this ).find( '.content-text, .bg-overlay' ).stop().fadeIn('slow');
		$( this ).find( '.content-text, .bg-overlay' ).stop().fadeTo( "slow", 1 );
	});
		
	$( "#rotator" ).slides({
		container: 'rotator-wrapper',
		play: true,
		pause: 2500,
		height: 496,
		preload: true,
		preloadImage:'wp-content/themes/sushi_2/images/loading2.GIF',	
		hoverPause: true,
		generatePagination: true,
		generateNextPrev: false,
		effect: 'fade',
		/* crossfade: true, */
		fadeSpeed: 2000
	});
	
	/*$( "#updates" ).slides({
		container: 'updates-wrapper',
		play: false,
		pause: 2500,
		height: 620,
		preload: true,
		preloadImage:'wp-content/themes/sushi/images/loading2.GIF',	
		hoverPause: false,
		generatePagination: false,
		generateNextPrev: true,
		effect: 'fade',
		fadeSpeed: 1000
	});*/
	
	$( '#searchform' ).hide();
	
	$( '#search img' ).on( "click", function() {
		var sf = $( '#searchform' );
		$( this ).animate({
			opacity: 0
		}, 1000 ).remove();
		$( sf ).stop().show().animate({
			opacity: 1
		}, 1000);
	});
	
	$( '#searchsubmit' ).on( "click", function() {
		if( $( '#searchform .field' ).val() == '' ) {
			return false;
		}
	});
	
	$( '.img-container.home .img-holder:nth-child(1)' ).addClass( 'first' );
	$( '.img-container.home .img-holder:nth-child(2)' ).addClass( 'second' );

	$( '#gal-images li' ).each( function(e) {
		if( e % 3 == 0 ) {
			$( this ).addClass( 'no-ml' );
		}
	});
	
	$( 'div#solutions div.sol-entry' ).each( function(e) {
		if( e % 2 == 0 ) {
			$( this ).addClass( 'no-ml' );
		}
	});
	
	$('ul#gal-images').easyPaginate({
		step: 9
	});
	
	$('.fancybox').fancybox({
		openEffect  : 'none',
		closeEffect	: 'none',

		helpers : {
			title : {
				type : 'over'
			}
		}
	});
		
	$('.fancybox-media')
	.attr('rel', 'media-gallery')
	.fancybox({
		openEffect : 'none',
		closeEffect : 'none',
		prevEffect : 'none',
		nextEffect : 'none',

		arrows : false,
		helpers : {
			media : {},
			buttons : {}
		}
	});
		
	$("a.fancybox-media").click(function() {
		$.fancybox({
				'padding'       : 0,
				'autoScale'     : false,
				'transitionIn'  : 'none',
				'transitionOut' : 'none',
				'title'         : this.title,
				'width'     : 330,
				'height'        : 330,
				'href'          : this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
				'type'          : 'swf',
				'swf'           : {
					 'wmode'        : 'transparent',
					'allowfullscreen'   : 'true'
				}
			});

		return false;
	});
	
	$( '.img-block-container .plant-solutions-provider, .img-block-container .plant-opti' ).wrapAll( '<div class="col1"></div>' );
	$( '.bldg-entry:eq(0), .bldg-entry:eq(1)' ).wrapAll( '<div class="col1"></div>' );
	$( '.buildings .col1' ).append( '<div class="clr"></div>' );
	
	var isVisible = true;
	
	$( '.btnShow' ).on( "click", function() {
		var content = $( '.platinum-partner .content' );
		
		if( isVisible ) {
			if( $( this ).text() == 'See more' ) {
				content.hide();
			} else {
				$( this ).text( 'See more' );
				content.hide();
			}
		} else {
			$( this ).text( 'Hide' );
			content.show();
		}
		isVisible = !isVisible;
	});
	
/* 	$( '.plant-solutions-provider .btn' ).on( "click", function() {
		console.log( 'btnShow' );
		var content = $( '.plant-solutions-provider .content' );
		if( isVisible ) {
			content.show();
			$( this ).each(function () {
				this.style.setProperty( 'bottom', '-39px', 'important' );
			});
		} else {
			content.hide();
			$( this ).each(function () {
				this.style.setProperty( 'bottom', '-69px', 'important' );
			});
		}
		isVisible = !isVisible;
	});
	
	$( '.plant-opti .btn' ).on( "click", function() {
		var content = $( '.plant-opti .content' );
		if( isVisible ) {
			content.show();
			content.css( 'margin-top', '10px' );
			$( this ).each(function () {
				this.style.setProperty( 'bottom', '-59px', 'important' );
			});
		} else {
			content.hide();
			$( this ).each(function () {
				this.style.setProperty( 'bottom', '-100px', 'important' );
			});
		}
		isVisible = !isVisible;
	});
	
	$( '.repairs .btn' ).on( "click", function() {
		var content = $( '.repairs .content' );
		if( isVisible ) {
			content.show();
			content.css( 'margin-top', '10px' );
			$( this ).each(function () {
				this.style.setProperty( 'bottom', '-92px', 'important' );
			});
		} else {
			content.hide();
			$( this ).each(function () {
				this.style.setProperty( 'bottom', '-147px', 'important' );
			});
		}
		isVisible = !isVisible;
	});
	
	$( '.aquisition .btn' ).on( "click", function() {
		var content = $( '.aquisition .content' );
		if( isVisible ) {
			content.show();
			content.css( 'margin-top', '10px' );
			$( this ).each(function () {
				this.style.setProperty( 'bottom', '-99px', 'important' );
			});
		} else {
			content.hide();
			$( this ).each(function () {
				this.style.setProperty( 'bottom', '-124px', 'important' );
			});
		}
		isVisible = !isVisible;
	}); */
	
	// Contact Form 7 Events
	$( ".wpcf7-form" ).ajaxComplete( function( event, request, settings ) {
		var $cf7 = $( event.target );
		
		if ( $cf7.hasClass( 'sent' ) ) {
			// form sent successfully
		} else if ( $cf7.hasClass( 'invalid' ) ) {
			// form has errors
		} else {
			// assume form has failed
		}
		
		$cf7.find( '.wpcf7-response-output' ).delay( 4000 ).fadeOut( 500 );
	});	
	
	// No click on active nav menu.
	$(".current_page_item > a").click( function(e) {
     		e.preventDefault();    
    }).css("cursor", "default");
	
	// Set phone validation on a field.
	validate_phone( "#contact-form7 #cf7-contactno" );		
	
	
	// Comments form validation: latest forums and single comments
	// swp_comments_validation(); <-- uncomment
	
	// IE Fix: Cursor not appearing after tab
	$( "input[type='text']" ).focus( function() {
		if ( $( this ).val() === '' )
			$( this ).val('').select();
	});
	
	// IE8 Fix: Centered banner image
	if ( $.BrowserInfo.Name == 'ie' && parseInt( $.BrowserInfo.FullVersion ) < 9 ) {		
		$( window ).resize( function() {
			var wd = $( window.document ).width();
			if ( wd < 1920 ) {
				var diff = ( 1920 - wd ) / 2;
				$( "#banner .page-banner .image-wrapper img" ).css( 'margin-left', -( diff / 2 ) + 'px' );
			} else {
				$( "#banner .page-banner .image-wrapper img" ).css( 'margin-left', 'auto' );
			}
		});		
	}
	
	//For hacking
	if ( navigator.userAgent.match( /ipad/i ) ) {
		$( "html" ).addClass( "ipad" );
	}
	
	if ( navigator.userAgent.match( /ipod/i ) ) {
		$( "html" ).addClass( "ipod" );
	}
	
	$('.bxslider').bxSlider({
		mode: 'vertical',
		nextSelector: jQuery( '#next2' ),
		prevSelector: jQuery( '#prev2' ),
		autoHover: true,
		pager: false,
		auto: true,
		moveSlides: 5000
	});
	
	if ( location.pathname == '/capabilities/' && location.hash != "" ) {
		if ( ! jQuery( location.hash ).length )
			return;
		
		jQuery( 'html, body' ).animate({
			scrollTop: jQuery( location.hash ).offset().top - 50
		}, 1000 );
	}

	/* $('.updates-wrapper').cycle({ 
		fx:     'scrollVert',
		speed:  500, 
		timeout: 0,
		next:   '#next2', 
		prev:   '#prev2' 
	}); */

});

function validate_phone( selector ) {
	jQuery( selector ).keydown( function( event ) {
		// Allow: backspace, delete, tab, escape, and enter
		if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
			 // Allow: Ctrl+A Select All
			(event.keyCode == 65 && event.ctrlKey === true) || 
			 // Allow: Ctrl+V Paste
			(event.keyCode == 86 && event.ctrlKey === true) ||
			 // Allow: home, end, left, right
			(event.keyCode >= 35 && event.keyCode <= 39)) {
				 // let it happen, don't do anything
				 return;
		}
		else {
			// Ensure that it is a number and stop the keypress
			if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault(); 
			}   
		}
	});
}

function swp_comments_validation()
{
	// Comment textbox focus after clicking comment button
	if ( location.hash == '#respond' ) {
		$( "#comment" ).focus();
	}
	
	$( "#frost-submit" ).click( function(e) {		
		
		var author = $( "#commentform #author" );
		var email = $( "#commentform #email" );
		var comment = $( "#commentform #comment" );
		var e1 = e2 = e3 = false;
		
		if ( author.length > 0 && author.val() == '' || author.attr( 'placeholder' ) === author.val() ) {
			e1 = true;			
			if ( ! $( "#commentform .comment-form-author .error" ).length ) {
				$( "#commentform .comment-form-author" ).append( $( "<span class='error hidden'>Please tell us your name.</span>" ) );
			} 			
		}
		
		if ( email.length > 0 && email.val() == '' || email.attr( 'placeholder' ) === email.val() ) {
			e2 = true;			
			if ( ! $( "#commentform .comment-form-email .error" ).length ) {
				$( "#commentform .comment-form-email" ).append( $( "<span class='error hidden'>Please provide your email.</span>" ) );
			}
		} else if ( email.length > 0 && ! is_email( email.val() ) ) {
			e2 = true;			
			if ( ! $( "#commentform .comment-form-email .error" ).length ) {				
				$( "#commentform .comment-form-email" ).append( $( "<span class='error hidden'>Email is not valid.</span>" ) );
			} else {
				$( "#commentform .comment-form-email .error" ).text( 'Email is not valid.' );
			}
		}
		
		if ( comment.length > 0 && comment.val() == '' || comment.attr( 'placeholder' ) === comment.val() ) {
			e3 = true;			
			if ( ! $( "#commentform .comment-form-comment .error" ).length ) {
				$( "#commentform .comment-form-comment" ).append( $( "<span class='error hidden'>Please say something.</span>" ) );
			} 		
		}
		
		$( "#commentform span.error" ).on( 'mouseover', function(e) {
			$( this ).stop().fadeOut( 600 );
			$( this ).prev().focus();
		});
		
		if ( e1 == true ) {
			e.preventDefault();
			$( "#commentform .comment-form-author .error" ).stop().show();
		}	

		if ( e2 == true ) {
			e.preventDefault();
			$( "#commentform .comment-form-email .error" ).stop().show();
		}
			
		if ( e3 == true ) {
			e.preventDefault();
			$( "#commentform .comment-form-comment .error" ).stop().show();
		}
	});
}

function is_email( email )
{ 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test( email );
} 

/*
* END OF FILE
* global.js
*/