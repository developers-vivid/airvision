<?php
/**
 * Template Name: Home
 *
 * @author Sushi Katana team
 * @copyright 2013 Sushi Digital Pty. Ltd.
 * @since Sashimi 3.0
 * @package WordPress
 * @subpackage Sushi_WP
 */
	get_header();
	// $page_object = get_queried_object();
	// $page_id     = get_queried_object_id();
?>
	<section id="banner" class="row">
		<div class="main-container">
			<div id="rotator">
				<div class="rotator-wrapper">
					<?php
						if( get_field( 'image_slider') ) :
							while( has_sub_field( 'image_slider') ) :
							
							$image = get_sub_field('image');
							$caption = get_sub_field('caption');		
					?>
					<style>
					.customSize img {
						width:100%;
						height:100%;
					}
					</style>
							<div class="slide customSize">
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $caption; ?>" title="<?php echo $caption; ?>" />
							</div>
					<?php
							endwhile;
						endif;
					?>
				</div>
			</div>
		</div>
	</section>
	<section id="content-area" class="row">
		<div class="main-container">
			<div class="main-content">
				<div class="content-text">
					<h1><?php echo get_field( 'header_text', 5 );?></h1>
					<?php 
						$postm = get_post( 5 );
						$npost = $postm->post_content;
					?>
					<?php //<p><?php echo $npost ?.></p>  ?>
				</div>
			</div>

			<?php
				/*query_posts( 'p=125' );
				while( have_posts() ) : the_post();*/
				$brandnames = get_field( 'brand_names', 5 );
				$brandlink  = get_field( 'brand_button_link', 5 );
			?>
				<div class="leading-edge-tech">
					<?php if( $brandnames ): 
							$ctr = 0;
							$brandcnt = count($brandnames);
							
							foreach ( $brandnames as $value ) : $ctr++; ?>
								<?php if( $value['brand_name'] == 'PowerPax' ){ ?>
											<h3  <?php //echo ( $ctr == $brandcnt )? 'class="brandheading4 no-pr"':'class="mLeft"'; ?>><?php echo $value['brand_name']; ?></h3>
								<?php }else{ ?>
											<h3 <?php //echo ( $ctr == $brandcnt )? 'class="brandheading4 no-pr"':''; ?>><?php echo $value['brand_name']; ?></h3>
								<?php } ?>
					<?php 	endforeach;
						   endif; ?>
					<?php /*<div class="content-text">
						<?php the_excerpt(); ?>
						<a href="<?php echo get_permalink( 125 ); ?>" class="see-more">Find out more</a>
					</div>*/ ?>
					<div class="clr"></div>
					<a href="<?php echo ( $brandlink )? $brandlink:''; ?>" class="see-more-lnk">See more</a>
				</div>
			<?php 
				/*endwhile; 
				wp_reset_query();*/
			?>

			<!-- platinum-partner was here -->

			<div class="platinum-service-partner">
				<?php //<h3><?php echo get_field( 'platinum_service_partner_title' ); ?.></h3> ?>
				<div class="img-container home">
					<?php
						if( get_field( 'platinum_service_partner', 5 ) ) :
							while( has_sub_field( 'platinum_service_partner', 5 ) ) :
							$image = get_sub_field( 'image' );
							if( !empty( $image ) ) :
					?>
							<div class="img-holder">				
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" title="<?php echo $image['alt']; ?>" />
							</div>
					<?php
							endif;
							endwhile;
					endif;
					?>
					<div class="clr"></div>
				</div>
			</div>

			

			<div class="platinum-partner">
					<h2><span class="company-name">Airvision</span> a <?php echo get_field( 'powerpax_platinum_partner_title', 5 ); ?></h2>
					<div class="content">
						<p><?php echo get_field( 'powerpax_platinum_partner_content', 5 ); ?></p>
					</div>
					<a href="<?php echo get_permalink(11); ?>" class="see-more-lnk"><!-- <span class="see-more-btn"> -->See more<!-- </span> --></a>
					<?php //<span class="btnShow">See more</span> ?>
			</div>
			
			<div class="img-block-container">
				<?php
					if( get_field( 'solutions', 5 ) ) :
						while( has_sub_field( 'solutions', 5 ) ) :
						$image = get_sub_field( 'solution_image' );
						$soltitle = get_sub_field( 'solution_title' );
				?>
						<div class="column <?php 
							switch( get_sub_field( 'solution_title' ) ) {
								case 'Total Central Plant Solutions Provider':
									echo 'plant-solutions-provider';
									break;
								case 'Central Plant Optimisation':
									echo 'plant-opti';
									break;
								case 'Chiller Repairs & Maintenance':
									echo 'repairs left';
									break;
								case 'Commissioning Service & Maintenance':
									echo 'repairs left'; 
									break;
								case 'System Data Acquisition & Trending':
									echo 'aquisition right';
									break;
								default:
									echo 'Invalid input.';
							}
						?>">
							<div class="content-text">
								<?php if( get_sub_field( 'solution_title' ) != '' ) : ?>
									<a href="<?php echo home_url( '/capabilities/#' . sanitize_title( $soltitle ) ); ?>">
										<h3><?php echo get_sub_field( 'solution_title' ); ?></h3>
									</a>
									<div class="content">
										<p><?php echo get_sub_field( 'solution_content' ); ?></p>
									</div>
									
								<?php endif; ?>
							</div>
							<a href="<?php echo home_url( '/capabilities/#' . sanitize_title( $soltitle ) ); ?>">
							<div class="bg-overlay"></div>
							<?php if( !empty( $image ) ) : ?>
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" title="<?php echo $image['alt']; ?>" />
							<?php endif; ?>
							</a>
						</div>
				<?php
						endwhile;
					endif;
				?>
				<div class="clr"></div>
			</div>
			<div class="contact-column">
				<?php
					if( get_field( 'contact_column', 5 ) ) :
						while( has_sub_field( 'contact_column', 5 ) ) :
						$image = get_sub_field( 'image' );
				?>
				<div class="col <?php if( $image != null ) { echo 'medium-level-bldg'; } ?>">
					<?php echo (get_sub_field('link')) ? '<a href="'.get_sub_field('link') .'">' : ''; ?>
					<div class="content-text">
						<p><?php echo get_sub_field( 'title' ); ?></p>
						<h3><?php echo get_sub_field( 'content' ); ?></h3>
						<?php 
							if( get_sub_field( 'title' ) == 'Need our services?' ) :		
						?>
								<a href="<?php echo site_url(); ?>/contact" class="btn">Go</a>
						<?php 
							endif;
						?>
					</div>
					<?php
						if( get_sub_field( 'image' ) ) :
					?>
						<div class="bg-overlay"></div>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" title="<?php echo $image['alt']; ?>" />
					<?php
						endif;
					?>
					<?php echo (get_sub_field('link')) ? '</a>' : '' ; ?>
				</div>
				<?php
						endwhile;
					endif;
				?>
				<div class="clr"></div>
			</div>
			<?php get_template_part( 'content-here-to-help' ); ?>
		</div>
	</section>
	<?php get_template_part( 'content-bottom-area' ); ?>
<?php get_footer(); ?>