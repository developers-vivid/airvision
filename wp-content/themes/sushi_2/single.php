<?php
/**
 * Sushi Wordpress Starter 3.0 Theme single template.
 *
 * @author Sushi Katana team
 * @copyright 2013 Sushi Digital Pty. Ltd.
 * @since Sashimi 3.0
 * @package WordPress
 * @subpackage Sushi_WP
 */
	get_header();

?>
	<section id="banner" class="contact row">
		<div class="main-container">
		<!--
			<?php 
				if( have_posts() ) :
					while( have_posts() ) : the_post();
			?>
						<?php 
							if( get_field( 'page_banner_text', 348 ) ) :
								while( has_sub_field( 'page_banner_text', 348 ) ) :
						?>
									<div class="text-overlay">
										<h2><?php echo get_sub_field( 'title' ); ?></h2>
										<?php echo apply_filters( 'the_content', get_sub_field( 'content' ) ); ?>
									</div>
						<?php
								endwhile;
							endif;
						?>
						<?php //if( has_post_thumbnail() ) :  ?>
							<div class="img-holder">
								<?php
									$img_id = get_post_meta( 348, '_thumbnail_id', true );
									$alt_text = get_post_meta( $img_id , '_wp_attachment_image_alt', true);
									$image_url = wp_get_attachment_image_src( $img_id, 'full' );
									//echo get_timthumb_img( array( 'alt' => $alt_text, 'title' => $alt_text ), array( 'src' => $image_url[0], 'w' => 1136, 'h' => 338 ) );
								?>
							</div>
						<?php //endif; ?>
			<?php
					endwhile;
					wp_reset_query();
				endif; 
			?>
		-->	
		<div class="text-overlay">
		<?php echo get_field('single_post_header_title', 174); ?>
		</div>	
		<style>
		.img-holder img{
		margin-bottom:-15px !important;
		}
		</style>		
		<div class="img-holder">
		<?php echo get_field('single_post_header_image', 174); ?>
		</div>		
		</div>
	</section>
	 <section id="content-area" class="row">
		<div class="main-container">
			<div class="main-content single-post">
				<?php
					if( have_posts() ) :
						while( have_posts() ) : the_post();
							the_title( '<h1>', '</h1>' );
				?>
							<div class="date"><?php the_time( 'M d, Y' ); ?></div>
							<div class="post-entry">
							<style>
							.customimagesize img{
								width:490px;
								height:250px;
							}
							</style>
				<?php       if( has_post_thumbnail() ) : ?>
								<div class="img-holder left customimagesize" style="width: 50%; float:left;">
									<?php
										//$img_id    = get_post_meta( get_the_ID(), '_thumbnail_id', true );
										//$alt_text  = get_post_meta( $img_id , '_wp_attachment_image_alt', true);
										//$image_url = wp_get_attachment_image_src( $img_id, 'full' );
										//echo get_timthumb_img( array( 'alt' => $alt_text, 'title' => $alt_text ), array( 'src' => $image_url[0], 'w' => 330, 'h' => 330 ) );
										//echo get_timthumb_img( array( 'alt' => $alt_text, 'title' => $alt_text ), array( 'src' => $image_url[0], 'w' => 490, 'h' => 250, 'zc' => 2 ) );
										//echo '<img src="'.$image_url[0].'" alt="'.$alt_text.'" title="'.$alt_text.'" />';
										the_post_thumbnail();
									?>
								</div>
				<?php       else: ?>				
								<div class="img-holder default left" style="width: 50%; float:left;">
									<img src="<?php echo get_bloginfo('template_url') ?>/images/Air-Vision-Logo.png" alt="AirVision" title="AirVision">
								</div>
				<?php       endif; ?>

							    <div class="left single-content" style="width: 47%; float:left;"><?php the_content(); ?></div>
							    <div class="clr"></div>
							</div>
				<?php
						endwhile;
					else :
				?>
						<h1 class="search"><?php _e( 'Nothing Found', 'sushi' ); ?></h1>
						<p><?php _e('Sorry, but nothing matched your search criteria. Please try again with some different keywords.'); ?></p>
				<?php
					endif;
				?>
				<br>
				<div class="nav_links">
					<!-- <div class="left prev"><?php  previous_post_link('%link', 'Previous', TRUE); ?></div>
					<div class="right next"><?php next_post_link('%link', 'Next', TRUE); ?></div> -->
					<div class="left prev"><?php next_post_link('%link', 'Previous', TRUE); ?></div>
					<div class="right next"><?php  previous_post_link('%link', 'Next', TRUE); ?></div>
					<div class="clr"></div>
				</div>

				<!-- Recommended Reading -->

				<!--<div class="recommended-reading">
					<h3 class="section-title">Recommended Readings</h3>
					<div class="line"></div>
					<?php
						$args = array(
									'post_type'      => 'post',
									'post_status'    => 'publish',
									'posts_per_page' => 6,
									'cat'            => 5
								);

						$loop = new WP_Query( $args );
						$ctr  = 0;
						
						if( $loop->have_posts() ) :
							while( $loop->have_posts() ): $loop->the_post(); $ctr++;
					?>
								<div class="rreading-article left <?php echo ( $ctr%3 == 0 )? 'no-mr':''; ?>">
									<a href="<?php echo get_permalink($post->ID); ?>">
									<?php if( has_post_thumbnail() ) :  ?>
										<div class="img-holder">
											<?php
												$img_id = get_post_meta( $post->ID, '_thumbnail_id', true );
												$alt_text = get_post_meta( $img_id , '_wp_attachment_image_alt', true);
												$image_url = wp_get_attachment_image_src( $img_id, 'full' );
												echo get_timthumb_img( array( 'alt' => $alt_text, 'title' => $alt_text ), array( 'src' => $image_url[0], 'w' => 320, 'h' => 159 ) );
											?>
										</div>
									<?php else: ?>
										<div class="img-holder default">
											<img src="<?php echo get_bloginfo('template_url') ?>/images/AirVision-Logo.png" alt="AirVision" title="AirVision">
										</div>		
									<?php endif; ?>	
									</a>
									<div class="article-title"><a href="<?php echo get_permalink($post->ID); ?>"><?php the_title(); ?></a></div>
									<div class="article-date"><?php the_time( 'M d, Y' ); ?></div>
								</div>
				    <?php   endwhile; wp_reset_postdata();
				    	endif; ?>
				    	<div class="clr"></div>
				</div>-->

				<!-- Recommended Reading -->
			</div>

			

			<div class="clr"></div>
			<?php get_template_part( 'content-here-to-help' ); ?>
		</div>
	</section>
	<?php get_template_part( 'content-bottom-area' ); ?>
<?php get_footer(); ?>