<div class="power-pax-img">
	<?php 
		if( get_field( 'powerpax_images', $page_id ) ) :
			while( has_sub_field( 'powerpax_images', $page_id ) ) :
				$img = get_sub_field( 'image' );
				$attributes = array( "alt"=> $img['alt'] , "title"=> $img['title'] );
				$params = array('src'=> $img['url'] , 'w'=>'317', 'h'=>'189', 'zc'=> '1');
				 if( $img ) :
	?>
				<div class="ppi-img left">
					<?php echo get_timthumb_img($attributes,$params); ?>
					<!--<img src="<?php //echo $img['url']; ?>" alt="<?php //echo $img['alt']; ?>" title="<?php //echo $img['alt']; ?>" width="317"  />-->
				</div>
	<?php
				endif;
			endwhile;
		endif;
	?>
	<div class="clr"></div>
</div>