<?php
/**
 * Template Name: Page inner
 *
 * @author Sushi Katana team
 * @copyright 2013 Sushi Digital Pty. Ltd.
 * @since Sashimi 3.0
 * @package WordPress
 * @subpackage Sushi_WP
 */
	get_header();
	
	$page_object = get_queried_object();
	$page_id  = get_queried_object_id(); // Get current page id
?>
	<section id="banner" class="row">
		<div class="main-container">
		<?php 
			if( have_posts() ) :
				while( have_posts() ) : the_post();
		?>
					<?php 
						if( get_field( 'page_banner_text', $page_id ) ) :
							while( has_sub_field( 'page_banner_text', $page_id ) ) :
					?>
								<div class="text-overlay">
									<h2><?php echo get_sub_field( 'title' ); ?></h2>
									<?php echo apply_filters( 'the_content', get_sub_field( 'content' ) ); ?>
								</div>
					<?php
							endwhile;
						endif;
					?>
					<?php if( has_post_thumbnail() ) :  ?>
						<div class="img-holder">
							<?php
								$img_id = get_post_meta( get_the_ID(), '_thumbnail_id', true );
								$alt_text = get_post_meta( $img_id , '_wp_attachment_image_alt', true);
								$image_url = wp_get_attachment_image_src( $img_id, 'full' );
								echo get_timthumb_img( array( 'alt' => $alt_text, 'title' => $alt_text ), array( 'src' => $image_url[0], 'w' => 1136, 'h' => 338 ) );
							?>
						</div>
					<?php endif; ?>
		<?php
				endwhile;
				wp_reset_query();
			endif; 
		?>
		</div>
	</section>
	<section id="content-area" class="row">
		<div class="main-container">
			<div class="main-content">
				<?php 
					if( have_posts() ) :
						while( have_posts() ) : the_post();
				?>
							<h1><?php the_title(); ?></h1>
				<?php 
							if( get_field( 'page_inner_sub_heading' ) != '' ) :
				?>
							<h3><?php echo get_field( 'page_inner_sub_heading' ); ?></h3>
				<?php
							endif;
								if( get_field( 'page_inner_images', $page_id ) ) :
				?>
							<div class="img-container left">
				<?php
									while( has_sub_field( 'page_inner_images', $page_id ) ) :
										$image = get_sub_field( 'image' );
				?>
										<div class="img-holder">
				<?php 
											if( is_page( 'about-us' ) ) { 
												echo get_timthumb_img( array( 'alt' =>  $image['alt'], 'title' =>  $image['alt'] ), array( 'src' => $image['url'], 'w' => 400, 'h' => 267 ) );
											} elseif( is_page( 'powerpax-partner' ) ) {
												echo get_timthumb_img( array( 'alt' =>  $image['alt'], 'title' =>  $image['alt'] ), array( 'src' => $image['url'], 'w' => 432, 'h' => 437 ) );
											}
				?>
											
										</div>
				<?php
									endwhile;
				?>
							</div>
				<?php
								endif;
							the_content(); 
				?>
							<div class="clr"></div>
				<?php
						endwhile;
						wp_reset_query();
					endif;
				?>
			</div>
			<?php 
				if( is_page( 'about-us' ) ) {
					get_template_part( 'content-about-us-buildings' ); 
					get_template_part( 'content-here-to-help' );
				} elseif( is_page( 'smardt-powerpax' ) ) {
					get_template_part( 'content-powerpax' );
					get_template_part( 'content-here-to-help' );			
				}
			?>
		</div>
	</section>
	<?php
		get_template_part( 'content-bottom-area' );
	?>
<?php get_footer(); ?>