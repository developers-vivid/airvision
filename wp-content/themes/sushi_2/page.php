<?php
/**
 * Sushi Wordpress Starter 3.0 Theme default page template.
 *
 * @author Sushi Katana team
 * @copyright 2013 Sushi Digital Pty. Ltd.
 * @since Sashimi 3.0
 * @package WordPress
 * @subpackage Sushi_WP
 */
	get_header();
?>
	<section id="banner" class="row">
		<?php 
			if( have_posts() ) :
				while( have_posts() ) : the_post();
					if( has_post_thumbnail() ) : 
		?>
						<div class="img-holder left">
							<?php
								$img_id = get_post_meta( get_the_ID(), '_thumbnail_id', true );
								$alt_text = get_post_meta( $img_id , '_wp_attachment_image_alt', true);
								$image_url = wp_get_attachment_image_src( $img_id, 'full' );
								echo get_timthumb_img( array( 'alt' => $alt_text, 'title' => $alt_text ), array( 'src' => $image_url[0], 'w' => 419, 'h' => 175 ) );
							?>
						</div>
		<?php
					endif;
				endwhile;
				wp_reset_query();
			endif; 
		?>
	</section>
	<section id="content-area" class="row">
		<div class="main-container">
			<div class="main-content">
				<?php 
					if( have_posts() ) :
						while( have_posts() ) : the_post();
				?>
							<h1><?php the_title(); ?></h1>
							<?php the_content(); ?>
				<?php
						endwhile;
						wp_reset_query();
					endif;
				?>
			</div>
		</div>
	</section>
<?php get_footer(); ?>