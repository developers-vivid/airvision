<?php
/**
 * Template Name: powerpax
 *
 * @author Sushi Katana team
 * @copyright 2013 Sushi Digital Pty. Ltd.
 * @since Sashimi 3.0
 * @package WordPress
 * @subpackage Sushi_WP
 */
	get_header();
	
	$page_object = get_queried_object();
	$page_id  = get_queried_object_id(); // Get current page id
?>
		<section id="banner" class="row">
		<div class="main-container">
								<div class="text-overlay">
								<?php echo get_field('banner-title'); ?>
								</div>
								<style>
								.img-holder img{
									margin-bottom:-15px !important;
								}
								</style>
								<div class="img-holder">
								<?php echo get_field('pax-banner'); ?>
								</div>
		</div>
	</section>
	<section id="content-area" class="row">
		<div class="main-container">
			<div class="main-content">
			<?php
			// TO SHOW THE PAGE CONTENTS
			while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
			<div class="entry-content-page">
            <?php the_content(); ?> <!-- Page Content -->
			</div><!-- .entry-content-page -->

			<?php
			endwhile; //resetting the page loop
			wp_reset_query(); //resetting the page query
			?>			
			<div class="clr"></div>
			</div>
		<?php get_template_part( 'content-here-to-help' ); ?>	
		</div>
	</section>
	<?php get_template_part( 'content-bottom-area' ); ?>
<?php get_footer(); ?>